﻿using Intel.RealSense;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace IntelRealSense_FrameCapture
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
		void App_Startup(object sender, StartupEventArgs e)
		{
			string PIDNew = "";
			int fpsRgb = 30;
			string resolutionRgb = "640x480";
			int fpsStereo = 30;
			string resolutionStereo = "640x480";
			bool emitter = true;
			bool depthEnable = true;
			bool irEnable = true;
			bool irQuality = false; // true = Y16, false = Y8

			if(e.Args.Length >3 ) //participantID, fps rgb, resolution rgb, fps stereo, resolution stereo, IR emitter, depth record, ir record, ir quality
			{
				PIDNew = e.Args[0].ToString();

				fpsRgb = Convert.ToInt16(e.Args[1]);
				resolutionRgb = Convert.ToString(e.Args[2]);
				fpsStereo = Convert.ToInt16(e.Args[3]);
				resolutionStereo = Convert.ToString(e.Args[4]);

				try
				{
					emitter = Convert.ToBoolean(e.Args[5]);
				}
				catch (Exception)
				{

					// could not convert parameter to boolean
					emitter = true;
				}

				try
				{
					depthEnable = Convert.ToBoolean(e.Args[6]);
				}
				catch (Exception)
				{

					// could not convert parameter to boolean
					depthEnable = true;
				}

				try
				{
					irEnable = Convert.ToBoolean(e.Args[7]);
				}
				catch (Exception)
				{

					// could not convert parameter to boolean
					irEnable = true;
				}

				try
				{
					irQuality = Convert.ToBoolean(e.Args[8]);
				}
				catch (Exception)
				{

					// could not convert parameter to boolean
					irQuality = false;
				}


				// Specific Parameters
				CaptureWindow captureWindow = new CaptureWindow(true,PIDNew, fpsRgb,resolutionRgb,fpsStereo,resolutionStereo, emitter,depthEnable,irEnable,irQuality);
				captureWindow.Show();
			}
			else
			{
				// Default Parameters
				CaptureWindow captureWindow = new CaptureWindow(false);
				captureWindow.Show();
			}
		}
    }
}
