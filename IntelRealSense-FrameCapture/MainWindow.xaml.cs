﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Intel.RealSense;
using LSL;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;

namespace Intel.RealSense
{
    /**
     * This code will send frame markers to LSL so that videos that are recorded using the tool can be parsed effectively in post-processing.
     * We might need a better protoccol so that too much video is not recorded so that it is more efficient.
     * **/

    public partial class CaptureWindow : Window
    {
        // Intel RealSense Variables
        private Pipeline pipeline;
        private Config cfg;
        private Colorizer colorizer;
        //private Align align;
        private CancellationTokenSource tokenSource = new CancellationTokenSource();

		private CustomProcessingBlock processBlock;

        // Lab Streaming Layer Variables
        /**
         * Identifying Variables: Process ID; Stream Name; Type of Data; Sampling Rate
         * **/
        private const string guid = "33C3D35A-51FF-44D9-8F05-81E972FA2F62"; // Unique Process ID -- Pre-Generated

        private string lslStreamName = "Intel RealSense Camera";
        private string lslStreamType = "Frame ID";
        private double sampling_rate = liblsl.IRREGULAR_RATE; // Default Value
        
        private bool depthEnable_g = false;
        private bool irEnable_g = false;
        private bool irQuality_g = false;
        
        private liblsl.StreamInfo lslStreamInfo;
        private liblsl.StreamOutlet lslOutlet = null; // The Streaming Outlet
        private int lslChannelCount = 6; // Number of Channels to Stream by Default

        private const liblsl.channel_format_t lslChannelFormat = liblsl.channel_format_t.cf_double64; // Stream Variable Format

        private double[] sample; // Data Samples to be Pushed into LSL

        private const string defaultDirectory = ".\\Recordings"; // Where the recordings are Stashed. TOFIX change this before release
        private string fileRecording = "";

        private void ShowResolutionOptions()
        {

            FPSRGBListBox.Items.Add("15");
            FPSRGBListBox.Items.Add("30");
            FPSRGBListBox.Items.Add("60");

            ResolutionRGBListBox.Items.Add("320x180");
            ResolutionRGBListBox.Items.Add("320x240");
            ResolutionRGBListBox.Items.Add("424x240");
            ResolutionRGBListBox.Items.Add("640x360");
            ResolutionRGBListBox.Items.Add("640x480");
            ResolutionRGBListBox.Items.Add("848x480");
            ResolutionRGBListBox.Items.Add("960x540");
            ResolutionRGBListBox.Items.Add("1280x720");
            ResolutionRGBListBox.Items.Add("1920x1080");

            if (ResolutionRGBListBox.SelectedIndex<0)
            {
                ResolutionRGBListBox.SelectedIndex = 4; // Select the default value of 640x480 at the beginning.
            }

            if (FPSRGBListBox.SelectedIndex < 0)
            {
                FPSRGBListBox.SelectedIndex = 1; // Select the default value of 30 at the beginning.
            }


            FPSStereoListBox.Items.Add("15"); // both (resolutions differ)
            FPSStereoListBox.Items.Add("25");
            FPSStereoListBox.Items.Add("30");
            FPSStereoListBox.Items.Add("60");
            FPSStereoListBox.Items.Add("90");
            FPSStereoListBox.Items.Add("100");
            FPSStereoListBox.Items.Add("300");

            ResolutionStereoListBox.Items.Add("256x144");
            ResolutionStereoListBox.Items.Add("424x240");
            ResolutionStereoListBox.Items.Add("480x270");
            ResolutionStereoListBox.Items.Add("640x360");
            ResolutionStereoListBox.Items.Add("640x400");
            ResolutionStereoListBox.Items.Add("640x480");
            ResolutionStereoListBox.Items.Add("848x480");
            ResolutionStereoListBox.Items.Add("848x100");
            ResolutionStereoListBox.Items.Add("1280x720");
            ResolutionStereoListBox.Items.Add("1280x800");

            if (ResolutionStereoListBox.SelectedIndex < 0)
            {
                ResolutionStereoListBox.SelectedIndex = 5; // Select the default value of 640x480 at the beginning.
            }

            if (FPSStereoListBox.SelectedIndex < 0)
            {
                FPSStereoListBox.SelectedIndex = 2; // Select the default value of 30 at the beginning.
            }

        }

        private void UploadImage(System.Windows.Controls.Image img, VideoFrame frame, System.Windows.Media.PixelFormat pixformat)
        {
            Dispatcher.Invoke(new Action(() =>
            {
                if (frame.Width == 0) return;

                var bytes = new byte[frame.Stride * frame.Height];
                frame.CopyTo(bytes);
                var bs = BitmapSource.Create(frame.Width, frame.Height,
                                  300, 300,
                                  pixformat,
                                  null,
                                  bytes,
                                  frame.Stride);

                var imgSrc = bs as ImageSource;

                img.Source = imgSrc;
            }));
        }

		public CaptureWindow(bool autolink = false,string pIDValue="P1", int fpsRgb=30, string resolutionRgb="640x480", int fpsStereo = 30, string resolutionStereo = "640x480", bool emitter = true,bool depthEnable=true, bool irEnable=true, bool irQuality=false)
		{


			InitializeComponent();
            ShowResolutionOptions();
            this.idTextBox.Text = pIDValue;

            for (int i = 0; i < FPSRGBListBox.Items.Count; i++)
            {
                if (fpsRgb == Int16.Parse(FPSRGBListBox.Items[i].ToString()))
                {
                    FPSRGBListBox.SelectedIndex = i;
                    break;
                }
            }

            for (int i = 0; i < ResolutionRGBListBox.Items.Count; i++)
            {
                if (resolutionRgb == ResolutionRGBListBox.Items[i].ToString())
                {
                    ResolutionRGBListBox.SelectedIndex = i;
                    break;
                }
            }

            for (int i = 0; i < FPSStereoListBox.Items.Count; i++)
            {
                if (fpsStereo == Int16.Parse(FPSStereoListBox.Items[i].ToString()))
                {
                    FPSStereoListBox.SelectedIndex = i;
                    break;
                }
            }

            for (int i = 0; i < ResolutionStereoListBox.Items.Count; i++)
            {
                if (resolutionStereo == ResolutionStereoListBox.Items[i].ToString())
                {
                    ResolutionStereoListBox.SelectedIndex = i;
                    break;
                }
            }

            EmitterCheck.IsChecked = emitter;

            DepthCheck.IsChecked = depthEnable;
            //depthEnable_g = depthEnable;
            IRCheck.IsChecked = irEnable;
            //irEnable_g = irEnable;

            IRQuality.IsChecked = irQuality;
           //irQuality_g = irQuality;

            updateCfg();

            if (autolink)
            {
                LinkLabStreamingLayer();
            }
            
		}


        private bool updateCfg()
        {

            Regex resolutionRegex = new Regex(@"(?<width>\d+)x(?<height>\d+)");

            int widthRgb = 0;
            int heightRgb = 0;
            int fpsRgb = 0;

            int widthStereo = 0;
            int heightStereo = 0;
            int fpsStereo = 0;


            Match matchRGB = resolutionRegex.Match((string)ResolutionRGBListBox.SelectedItem);
            if (matchRGB.Success)
            {
                widthRgb = int.Parse(matchRGB.Groups["width"].Value);
                heightRgb = int.Parse(matchRGB.Groups["height"].Value);
            }

            fpsRgb = int.Parse((string)FPSRGBListBox.SelectedItem);


            Match matchStereo = resolutionRegex.Match((string)ResolutionStereoListBox.SelectedItem);
            if (matchStereo.Success)
            {
                widthStereo = int.Parse(matchStereo.Groups["width"].Value);
                heightStereo = int.Parse(matchStereo.Groups["height"].Value);
            }

            fpsStereo = int.Parse((string)FPSStereoListBox.SelectedItem);


            if (!verifyDepth() || !verifyIR())
            {
                return false;
            }

            infoTextBox.Text = "";

            cfg = new Config();
            cfg.EnableStream(Stream.Color, widthRgb, heightRgb, Format.Rgb8, fpsRgb);

            if (DepthCheck.IsChecked.Value)
            {
                cfg.EnableStream(Stream.Depth, widthStereo, heightStereo, Format.Z16, fpsStereo);
            }
            else
            {
                cfg.DisableStream(Stream.Depth);
            }
            if (IRCheck.IsChecked.Value)
            {
                Intel.RealSense.Format irFormat = Format.Y8;
                if (IRQuality.IsChecked.Value)
                {
                    irFormat = Format.Y16;
                }
                cfg.EnableStream(Stream.Infrared, widthStereo, heightStereo, irFormat, fpsStereo); //infrared is useless when the tobii pro is also in use (its IR emitter is too bright)
            }
            else
            {
                cfg.DisableStream(Stream.Infrared);
                //cfg.DisableStream(Stream.Infrared,2);
            }
            checkDirectory();
            cfg.EnableRecordToFile(fileRecording + ".bag");

            depthEnable_g = DepthCheck.IsChecked.Value;
            irEnable_g = IRCheck.IsChecked.Value;
            irQuality_g = IRQuality.IsChecked.Value;

            return true;
        }

        /**
         * NOTES 
         * Curently it records immediately after linking the program with LabStreamLayer and there is a consumer of the data. 
         * There might be a better solution, but we don't want to increase the number of button presses for the protoccol. It is probably better to record more than to forget pressing 
         * the record button before an experiment. 
         * 
         * **/
        // Code Taken Directly from the LibRealSense 2 Examples -- Captures and Displays Depth and RGB Camera.
        private void startRecordingProcess()
        {
            try
            {

                pipeline = new Pipeline();

                colorizer = new Colorizer();
                //colorizer.Options.OptionValueDescription(Option.HistogramEqualizationEnabled, 1);
                //colorizer.Options.OptionValueDescription(Option.ColorScheme, 2);
                //align = new Align(Stream.Color);

                var pp = pipeline.Start(cfg);
                pp.Device.As<RecordDevice>().Pause();

                foreach (Sensor s in pipeline.ActiveProfile.Device.QuerySensors())
                {
                    foreach (StreamProfile profile in s.StreamProfiles)
                    {
                        var video = profile.As<VideoStreamProfile>();
                        if (video != null)
                        {
                            Console.WriteLine("Name:{0}  Resolution:{1}x{2}   FPS:{3}  Format:{4}", profile.Stream.ToString(), video.Width, video.Height, profile.Framerate, profile.Format);
                        }
                        else
                        {
                            Console.WriteLine("Name:{0}  Resolution:N/A   FPS:{1}  Format:{2}", profile.Stream.ToString(), profile.Framerate, profile.Format);
                        }
                    }
                }


                // IR emitter emits a dot pattern which aids in determining the depth of flat/featureless surfaces
                updateEmitterEnable();


                processBlock = new CustomProcessingBlock((f, src) =>
                {
                    // We create a FrameReleaser object that would track
                    // all newly allocated .NET frames, and ensure deterministic finalization
                    // at the end of scope. 
                    using (var releaser = new FramesReleaser())
                    {

                        var frames = f.As<FrameSet>().DisposeWith(releaser);
                        src.FrameReady(frames);
                        /*
                        var colorFrame = frames[Stream.Color].DisposeWith(releaser);
                        //var depthFrame = frames[Stream.Depth].DisposeWith(releaser);
                        var infraredFrame = frames[Stream.Infrared].DisposeWith(releaser);

                        // Combine the frames into a single result
                        var res = src.AllocateCompositeFrame(depthFrame, colorFrame).DisposeWith(releaser);
                        // Send it to the next processing stage
                        src.FrameReady(res);
                        */
                    }
                });

                processBlock.Start(f =>
				{
                    using (var frames = f.As<FrameSet>())
                    {
                        double clock = liblsl.local_clock();

                        var color_frame = frames.ColorFrame.DisposeWith(frames);
                        UploadImage(imgColor, color_frame, PixelFormats.Rgb24);

                        sample[0] = color_frame.Number;
                        sample[1] = color_frame.Timestamp;
                        sample[2] = 0;
                        sample[3] = 0;
                        sample[4] = 0;
                        sample[5] = 0;

                        if (depthEnable_g)
                        {
                            var depth_frame = frames.DepthFrame.DisposeWith(frames);
                            var colorized_depth = colorizer.Process<VideoFrame>(depth_frame).DisposeWith(frames);
                            UploadImage(imgDepth, colorized_depth, PixelFormats.Rgb24);
                            sample[2] = depth_frame.Number;
                            sample[3] = depth_frame.Timestamp;
                        }
                        if (irEnable_g)
                        {
                            var infrared_frame = frames.InfraredFrame.DisposeWith(frames);
                            
                            sample[4] = infrared_frame.Number;
                            sample[5] = infrared_frame.Timestamp;
                            if (!depthEnable_g)
                            {
                                System.Windows.Media.PixelFormat irFormat = PixelFormats.Gray8;
                                if (irQuality_g)
                                {
                                    irFormat = PixelFormats.Gray16;
                                }

                                UploadImage(imgDepth, infrared_frame, irFormat);
                            }
                                
                        }
                        

                        if (lslOutlet != null & lslOutlet.have_consumers())
						{

                            //rosbag record resume
                            pp.Device.As<RecordDevice>().Resume();

                            // Do LSL Streaming Here
                            lslOutlet.push_sample(sample, clock);
						}
                        else
                        {
                            //rosbag record pause
                            pp.Device.As<RecordDevice>().Pause();
                        }
					}
				});


                var token = tokenSource.Token;

                var t = Task.Factory.StartNew(() =>
                {
                    // Main Loop -- 
                    while (!token.IsCancellationRequested)
                    {
                        using (var frames = pipeline.WaitForFrames())
						{
							processBlock.Process(frames);
						}
                    }

                }, token);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Application.Current.Shutdown();
            }
        }

        private void LinkLabStreamingLayer()
        {

            if (!updateCfg())
            {
                return;
            }

            if (lslOutlet == null)
            {
                sample = new double[lslChannelCount];

                lslStreamInfo = new liblsl.StreamInfo(lslStreamName + "-" + idTextBox.Text, lslStreamType, lslChannelCount, sampling_rate, lslChannelFormat, guid + "-" + idTextBox.Text);
                liblsl.XMLElement chns = lslStreamInfo.desc().append_child("channels");
                chns.append_child("channel").append_child_value("label", "color_frame_id").append_child_value("unit", "number").append_child_value("type", "frameID");
                chns.append_child("channel").append_child_value("label", "color_frame_time").append_child_value("unit", "seconds").append_child_value("type", "timestamp");
                chns.append_child("channel").append_child_value("label", "depth_frame_id").append_child_value("unit", "number").append_child_value("type", "frameID");
                chns.append_child("channel").append_child_value("label", "depth_frame_time").append_child_value("unit", "seconds").append_child_value("type", "timestamp");
                chns.append_child("channel").append_child_value("label", "infrared_frame_id").append_child_value("unit", "number").append_child_value("type", "frameID");
                chns.append_child("channel").append_child_value("label", "infrared_frame_time").append_child_value("unit", "seconds").append_child_value("type", "timestamp");
                lslOutlet = new liblsl.StreamOutlet(lslStreamInfo);

                infoTextBox.Text += "Linked to Lab Streaming Layer\nNow Streaming Frame Data\n";
            }
            
            // Once linked no need for the button functionality so disable it.
            lslLink.Content = "Camera is Linked";
            lslLink.IsEnabled = false;

            // Disable the Experiment ID Text Functionality
            idTextBox.IsEnabled = false;

            ResolutionRGBListBox.IsEnabled = false;
            ResolutionStereoListBox.IsEnabled = false;
            FPSRGBListBox.IsEnabled = false;
            FPSStereoListBox.IsEnabled = false;
            //EmitterCheck.IsEnabled = false;

            DepthCheck.IsEnabled = false;
            IRCheck.IsEnabled = false;
            IRQuality.IsEnabled = false;

            startRecordingProcess();
        }

        private void checkDirectory()
        {
            if (!Directory.Exists(defaultDirectory))
            {
                Directory.CreateDirectory(defaultDirectory);
            }

            int filenum = 0;

            Regex fileRegex = new Regex(@"(?<participant>P\d+)_(?<filenum>\d+)\.bag");

            string participant = "";
            int filenum_other = 0;

            foreach (string f in Directory.GetFiles(defaultDirectory))
            {

                Match matchfile = fileRegex.Match(f);
                if (matchfile.Success)
                {
                    participant = matchfile.Groups["participant"].Value;
                    filenum_other = int.Parse(matchfile.Groups["filenum"].Value);
                }

                if (participant == idTextBox.Text && filenum_other >= filenum)
                {
                    filenum = filenum_other + 1;

                }

            }
            fileRecording = defaultDirectory + "\\" + idTextBox.Text + "_" + filenum;
            infoTextBox.Text += "Recording File = " + fileRecording + ".bag\n";

        }

        private static ImageCodecInfo GetEncoderInfo(String mimeType)
        {
            int j;
            ImageCodecInfo[] encoders;
            encoders = ImageCodecInfo.GetImageEncoders();
            for (j = 0; j < encoders.Length; ++j)
            {
                if (encoders[j].MimeType == mimeType)
                    return encoders[j];
            }
            return null;
        }

        private void updateEmitterEnable()
        {
            if (pipeline == null)
                return;
            var depth_sensor = pipeline.ActiveProfile.Device.QuerySensors().First(s => s.Is(Extension.DepthSensor));
            if (depth_sensor.Options.Supports(Option.EmitterEnabled))
            {

                if (EmitterCheck.IsChecked.Value)
                {
                    depth_sensor.Options[Option.EmitterEnabled].Value = 1f; // Enable emitter
                }
                else
                {
                    depth_sensor.Options[Option.EmitterEnabled].Value = 0f; // Disable emitter
                }

            }
        }

        private void EmitterCheck_checked(object sender, RoutedEventArgs e)
        {
            updateEmitterEnable();

        }

        private bool verifyFramerateResolution()
        {
            if (FPSStereoListBox == null || ResolutionStereoListBox == null)
            {
                return false;
            }

            if (FPSStereoListBox.SelectedItem.ToString() == "25" &&
              (ResolutionStereoListBox.SelectedItem.ToString() != "640x400" && ResolutionStereoListBox.SelectedItem.ToString() != "1280x800"))
            {
                Console.WriteLine("Stereo: Framerate of 25 is only compatible with resolution 640x400 or 1280x800");
                infoTextBox.Text = "Stereo: Framerate of 25 is only compatible with resolution 640x400 or 1280x800";
                return false;
            }
            else if ((FPSStereoListBox.SelectedItem.ToString() != "15" && FPSStereoListBox.SelectedItem.ToString() != "25" ) &&
              (ResolutionStereoListBox.SelectedItem.ToString() == "640x400" || ResolutionStereoListBox.SelectedItem.ToString() == "1280x800"))
            {
                Console.WriteLine("Stereo: Framerate of {0} is not compatible with resolution 640x400 or 1280x800", FPSStereoListBox.SelectedItem.ToString());
                infoTextBox.Text = String.Format("Stereo: Framerate of {0} is not compatible with resolution 640x400 or 1280x800", FPSStereoListBox.SelectedItem.ToString());
                return false;
            }
            else
            {
                return true;
            }
        }

        private bool verifyDepth()
        {
            if (!verifyFramerateResolution())
            {
                return false;
            }

            if (DepthCheck.IsChecked.Value &&
                (FPSStereoListBox.SelectedItem.ToString() == "25" ||
               ResolutionStereoListBox.SelectedItem.ToString() == "640x400" || ResolutionStereoListBox.SelectedItem.ToString() == "1280x800"))
            {
                Console.WriteLine("Stereo: Depth is not available with framerate 25 or resolution 640x400 or 1280x800");
                infoTextBox.Text = "Stereo: Depth is not available with framerate 25 or resolution 640x400 or 1280x800";
                //DepthCheck.IsChecked = false;
                return false;
            }
            else
            {
                return true;
            }
        }
        private void DepthCheck_checked(object sender, RoutedEventArgs e)
        {
 
            if (DepthCheck.IsChecked.Value && !verifyDepth())
            {
                DepthCheck.IsChecked = false;
            }
            else if (DepthCheck.IsChecked.Value)
            {
                EmitterCheck.IsEnabled = true;
            }
            else
            {
                EmitterCheck.IsChecked = false;
                EmitterCheck.IsEnabled = false;
            }


            
        }

        private void IRCheck_checked(object sender, RoutedEventArgs e)
        {

            if (IRCheck.IsChecked.Value && verifyIR())
            {
                //IRQuality.IsEnabled = true;
            }
            else if (IRCheck.IsChecked.Value && !verifyIR())
            {
                IRCheck.IsChecked = false;
            }
            else if (IRQuality != null)
            {
                //IRQuality.IsEnabled = false;
            }

        }

        private bool verifyIR()
        {
            if (!verifyFramerateResolution())
            {
                return false;
            }

            // Not all options are compatible with the Y16 or Y8 format for Infrared
            if (IRQuality.IsChecked.Value && 
               ((FPSStereoListBox.SelectedItem.ToString() != "15" && FPSStereoListBox.SelectedItem.ToString() != "25") ||
               (ResolutionStereoListBox.SelectedItem.ToString() != "640x400" && ResolutionStereoListBox.SelectedItem.ToString() != "1280x800")))
            {
                Console.WriteLine("Stereo: Infrared Y16 format is only available with framerate 15 or 25 and resolution 640x400 or 1280x800");
                infoTextBox.Text = "Stereo: Infrared Y16 format is only available with framerate 15 or 25 and resolution 640x400 or 1280x800";
                //IRQuality.IsChecked = false;
                return false;
            }
            else if (!IRQuality.IsChecked.Value &&
                (FPSStereoListBox.SelectedItem.ToString() == "25" ||
               ResolutionStereoListBox.SelectedItem.ToString() == "640x400" || ResolutionStereoListBox.SelectedItem.ToString() == "1280x800"))
            {
                Console.WriteLine("Stereo: Infrared Y8 format is not available with framerate 25 or resolution 640x400 or 1280x800");
                infoTextBox.Text = "Stereo: Infrared Y8 format is not available with framerate 25 or resolution 640x400 or 1280x800";
                //IRQuality.IsChecked = true;
                return false;
            }
            else
            {
                return true;
            }
        }

        private void IRQuality_checked(object sender, RoutedEventArgs e)
        {
            if (!verifyFramerateResolution())
            {
                return;
            }
            if (IRQuality.IsChecked.Value && !verifyIR())
            {
                IRQuality.IsChecked = false;
            }
            else if (!IRQuality.IsChecked.Value && !verifyIR())
            {
                IRQuality.IsChecked = true;
            }
            

        }

        // Interface Controls Go Here
        private void control_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
			tokenSource.Cancel();
			
			while (pipeline != null)
			{
				pipeline.Stop();
				pipeline = null;
			}
		}

        private void lslLink_Click(object sender, RoutedEventArgs e)
        {
            // Link LabStreamLayer
            LinkLabStreamingLayer();
        }


    }
}
