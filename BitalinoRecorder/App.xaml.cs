﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace BitalinoRecorder
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
		void App_Startup(object sender, StartupEventArgs e)
		{
			string PIDNew = "";
			string BitalinoMac = "";
			int ChannelMask = 0b11111;
			int samplingrate = 100;


			if(e.Args.Length > 0)
			{

				PIDNew = e.Args[0].ToString();

				if (e.Args.Length >1)
				{
					BitalinoMac = e.Args[1];

					if (e.Args.Length > 2 )
					{
						try
						{
							ChannelMask = Convert.ToInt32(e.Args[2], 2);
						}
						catch (Exception)
						{

							ChannelMask = 0b00000;
						}

						if (e.Args.Length > 3)
						{
							try
							{
								samplingrate = Convert.ToInt32(e.Args[3]);
							}
							catch (Exception)
							{

								// could not convert sampling rate from parameters
							}

						}

					}

				}	
				MainWindow mainWindow = new MainWindow(PIDNew, BitalinoMac, ChannelMask, samplingrate);
				mainWindow.Show();
			}
			else
			{
				MainWindow mainWindow = new MainWindow();
				mainWindow.Show();
			}
			
		}


    }
}
