﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace SensingTex_PressureMat
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
		void App_Startup(object sender, StartupEventArgs e)
		{
			string PIDNew = "";
			int com_port = 0;

			if (e.Args.Length > 0)
			{
				PIDNew = e.Args[0].ToString();

				if (e.Args.Length > 1)
				{
					try
					{
						com_port = Convert.ToInt32(e.Args[1]);
					}
					catch (Exception)
					{

						// could not convert parameter
					}
					
				}
				

				MainWindow mainWindow = new MainWindow(PIDNew, com_port);
				mainWindow.Show();
			}
			else
			{
				// Default Parameters
				MainWindow mainWindow = new MainWindow();
				mainWindow.Show();
			}
		}
	}
}
